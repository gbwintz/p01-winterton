//
//  main.m
//  p01-winterton
//
//  Created by Gabrielle Winterton on 1/29/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
