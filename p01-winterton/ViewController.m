//
//  ViewController.m
//  p01-winterton
//
//  Created by Gabrielle Winterton on 1/29/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloWorld;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)clickE:(id)sender
{
    [helloWorld setText:@"Hello World"];
}

-(IBAction)clickS:(id)sender
{
    [helloWorld setText:@"Hola Mundo"];
}

-(IBAction)clickF:(id)sender
{
    [helloWorld setText:@"Bonjour le monde"];
}

@end
