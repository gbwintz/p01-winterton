//
//  ViewController.h
//  p01-winterton
//
//  Created by Gabrielle Winterton on 1/29/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel  *helloWorld;

-(IBAction)clickE:(id)sender;

-(IBAction)clickS:(id)sender;

-(IBAction)clickF:(id)sender;

@end

