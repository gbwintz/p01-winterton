//
//  AppDelegate.h
//  p01-winterton
//
//  Created by Gabrielle Winterton on 1/29/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

